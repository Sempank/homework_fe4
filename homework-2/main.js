function Hamburger(size, stuffing) {
    try {
        if (arguments.length === 0 || arguments.length > 2)
            throw new Error('Неверное кол-во параметров');
        if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE)
            throw new HamburgerException('Не указан размер');
        if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO && stuffing !== Hamburger.STUFFING_SALAD)
            throw new HamburgerException('Не указана начинка');
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    } catch (err) {
        console.log(err.message);
    }
}
/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    size: 'small',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    size: 'large',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    stuffing: 'cheese',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    stuffing: 'salad',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    stuffing: 'potato',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    topping: 'mayo',
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    topping: 'spice',
    price: 15,
    calories: 0
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 */
function HamburgerException(message) {
    this.name = 'Hamburger Exception';
    this.message = message;
}

HamburgerException.prototype = Error.prototype;

/*
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (arguments.length === 0)
            throw new HamburgerException('Нет параметров');

        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] !== Hamburger.TOPPING_MAYO && arguments[i] !== Hamburger.TOPPING_SPICE)
                throw new HamburgerException('Неверное имя добавки');
        }
        for (var i = 0; i < arguments.length; i++) {
            for (var j = 0; j < arguments.length; j++) {
                if (arguments[i] === arguments[j] && j !== i)
                    throw new HamburgerException('Повторяющиеся топпинг в списке аргуметнов');
            }
        }

        if (this.toppings.indexOf(topping) !== -1)
            throw new HamburgerException('Топпинг ' + topping.topping + ' уже добавлен')

        for (var i = 0; i < arguments.length; i++)
            this.toppings.push(arguments[i]);

    } catch (err) {
        console.log(err.message);
    }
}
/*
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.toppings.includes(topping)) {
            return this.toppings.splice(this.toppings.indexOf(topping), 1);
        } else {
            throw new HamburgerException('Вы не можете убрать то, чего не добавляли!');
        }
    } catch (err) {
        console.log(err.message);
    }
}
/*
 * Получить список добавок.
 */

Hamburger.prototype.getToppings = function () {
    return this.toppings;
}
/*
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function () {
    return (this.size.size === 'small') ? Hamburger.SIZE_SMALL : Hamburger.SIZE_LARGE;
}

/*
 * Узнать начинку гамбургера
 */

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

/*
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */

Hamburger.prototype.calculatePrice = function () {
    var sizePrice = this.getSize().price;
    var stuffingPrice = this.getStuffing().price;
    var toppingsPrice = this.toppings.reduce(function (accum, curVal) {
        return accum + curVal.price;
    }, 0);
    return toppingsPrice + sizePrice + stuffingPrice;
}

/*
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */

Hamburger.prototype.calculateCalories = function () {
    var sizeCalories = this.getSize().calories;
    var stuffingCalories = this.getStuffing().calories;
    var toppingsCalories = this.toppings.reduce(function (accum, curVal) {
        return accum + curVal.calories;
    }, 0);
    return toppingsCalories + stuffingCalories + sizeCalories;
}
console.log('---------------------------------------------');


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


console.log('---------------------------------------------');

// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'