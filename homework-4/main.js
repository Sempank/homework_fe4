let body = document.getElementsByTagName("body")[0];
let table = document.createElement('table');
for (let i = 0; i < 30; i++) {
    let row = document.createElement('tr');
    for (let j = 0; j < 30; j++) {
        let cell = document.createElement('td');
        row.appendChild(cell);
    }
    table.appendChild(row);
}
body.appendChild(table);

body.addEventListener('click', function (e) {
    if (e.target.tagName === 'TD')
        e.target.classList.toggle('color-fill');
    if (e.target.tagName === 'BODY') {
        e.target.classList.toggle('reverse');
    }
});