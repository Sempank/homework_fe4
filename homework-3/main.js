'use strict';
class Hamburger {
    constructor(size, stuffing) {
        try {
            if (arguments.length === 0 || arguments.length > 2)
                throw new Error('Неверное кол-во параметров');
            if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE)
                throw new HamburgerException('Не указан размер');
            if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO && stuffing !== Hamburger.STUFFING_SALAD)
                throw new HamburgerException('Не указана начинка');
            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
        } catch (err) {
            console.log(err);
        }
    }

    addTopping(topping) {
        try {
            if (arguments.length === 0)
                throw new HamburgerException('Нет параметров');

            for (let i = 0; i < arguments.length; i++)
                if (arguments[i] !== Hamburger.TOPPING_MAYO && arguments[i] !== Hamburger.TOPPING_SPICE)
                    throw new HamburgerException('Неверное имя добавки');
            for (let i = 0; i < arguments.length; i++)
                for (let j = 0; j < arguments.length; j++)
                    if (arguments[i] === arguments[j] && j !== i)
                        throw new HamburgerException('Повторяющиеся топпинг в списке аргуметнов');
            if (this.toppings.includes(topping))
                throw new HamburgerException('Топпинг ' + topping.topping + ' уже добавлен')
            for (let i = 0; i < arguments.length; i++)
                this.toppings.push(arguments[i]);
        } catch (err) {
            console.log(err.message);
        }
    }

    removeTopping(topping) {
        try {
            if (this.toppings.includes(topping))
                return this.toppings.splice(this.toppings.indexOf(topping), 1);
            else
                throw new HamburgerException('Вы не можете убрать то, чего не добавляли!');
        } catch (err) {
            console.log(err.message);
        }
    }

    getToppings() {
        return this.toppings;
    }

    getSize() {
        return (this.size.size === 'small') ? Hamburger.SIZE_SMALL : Hamburger.SIZE_LARGE;
    }

    getStuffing() {
        return this.stuffing;
    }


    calculatePrice() {
        let sizePrice = this.getSize().price;
        let stuffingPrice = this.getStuffing().price;
        let toppingsPrice = this.toppings.reduce((accum, curVal) => accum + curVal.price, 0);
        return toppingsPrice + sizePrice + stuffingPrice;
    }


    calculateCalories() {
        let sizeCalories = this.getSize().calories;
        let stuffingCalories = this.getStuffing().calories;
        let toppingsCalories = this.toppings.reduce((accum, curVal) =>accum + curVal.calories, 0);
        return toppingsCalories + stuffingCalories + sizeCalories;
    }


    static SIZE_SMALL = {size: 'small', price: 50, calories: 20};
    static SIZE_LARGE = {size: 'large', price: 100, calories: 40};
    static STUFFING_CHEESE = {stuffing: 'cheese', price: 10, calories: 20};
    static STUFFING_SALAD = {stuffing: 'salad', price: 20, calories: 5};
    static STUFFING_POTATO = {stuffing: 'potato', price: 15, calories: 10};
    static TOPPING_MAYO = {topping: 'mayo', price: 20, calories: 5};
    static TOPPING_SPICE = {topping: 'spice', price: 15, calories: 0};

}


class HamburgerException extends Error {
    constructor(message) {
        super(message);
        this.name = 'Hamburger Exception';
        this.message = message;
    }
}

console.log('---------------------------------------------');
// console.log(h.getToppings);

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


console.log('---------------------------------------------');

// не передали обязательные параметры
let h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'